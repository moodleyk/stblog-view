<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>STB Information</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <script src="js/bootstrap.min.js" ></script>

    <!-- Custom styles for this template -->
    <link href="grid.css" rel="stylesheet">
  </head>

  <body>
    	<div class="container">
	<?php 
		$rootFolder = "logs/".findLatestLogFolder();
	?>
	<p class="lead">Searching in Folder Date: <?= $rootFolder ?>	</p>

	<table class="table">
	  <thead>
	    <tr>
	      	<th scope="col">#</th>
		<th scope="col">Box Name</th>
		<th scope="col">Latest Log</th>
		<th scope="col">Software Release</th>
		<th scope="col">Decoder Serial No#</th>
		<th scope="col">SmartCard No#</th>
		<th scope="col">SmartCard Nationality</th>
		<th scope="col">Network Type</th>
	    </tr>
	  </thead>
	  <tbody>
		<?php 

		    $networks = array(
				"networkIS20" => "IS20",
				"networkE36B" => "E36B"
			);
			foreach ($networks as $network_type) :
				$boxFolder = $rootFolder."/".$network_type;
				$boxListArray = listSTBBoxes($boxFolder);
			
 				//echo "checking folder :.".sizeof($boxListArray)." <br>";
				$count = 0;
				foreach ($boxListArray as $row) :
	 				echo "<tr>";
			  		echo "<th scope='row'>".++$count."</th>";
					writeSTBInformationRows($row,$boxFolder,$network_type);
					echo "</tr>";
	 			endforeach;
	 		endforeach;  
		?>
	   
	  </tbody>
	</table>


   

    </div> <!-- /container -->


<?php
	function getFolderDate() {
	    return "14-11-2017";
	}

	function findLatestLogFolder(){
		$rootFolder = "logs/";
		$folderList = array();
		$folderList = array_diff(scandir($rootFolder), array('..', '.'));
		natsort($folderList);
		arsort($folderList);
		$folderList = array_values($folderList);
		//$folderList = array_values($folderList);
		//echo "### OLDEST FOLDER: ".$folderList[sizeof($folderList)-1]."<BR>";
		//echo "### LATEST FOLDER: ".$folderList[0]."<BR>";
		return $folderList[0];
	}

	function writeSTBInformationRows($boxName,$searchFolder,$network){
		$fdate = getFolderDate();
		$path = $searchFolder."/".$boxName;
		$latestLogFile = $path."/".findLatestLogFile($path);
		$smartcardNumber = findSmartCardNumber($latestLogFile);
		$imwVersion = findIMWVersion($latestLogFile);
	 	$nationality = findNationality($latestLogFile);
		$firmwareVersion = findFirmwareVersion($latestLogFile);
		$stbSerialNumber = findSTBSerialNumber($latestLogFile);
		
		$details = array(
			"boxName" => $boxName,
			"latestLogFile" => $latestLogFile,
		    "software" => $firmwareVersion,
		    "serialNumber" => $stbSerialNumber,
		    "smartcardNumber" => $smartcardNumber,
		    "nationality" => $nationality,
		    "network" => $network
		);

		foreach ($details as $value) :
	 		echo "<td> $value </td>";
		endforeach;
	}

	function listSTBBoxes($searchFolder){
		$boxlist = array();
		$i = 0;
		echo "listSTBBoxes() :: ".$searchFolder."<br>";
		$boxlist = array_diff(scandir($searchFolder), array('..', '.'));
		sort($boxlist);
		return $boxlist;
	}



	function findSmartCardNumber($latestLogFile){
		$matches = array();
		$smartcardNumber = "";
		$lines = file($latestLogFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		
    	$matches = preg_grep("/sn=\d{10}/", $lines);
		$final_value = array();
		
		foreach ($matches as $value) {
			preg_match('/sn=\d{10}/', $value, $final_value);
			if (sizeof($final_value)>0){
				$smartcardNumber=explode("=",$final_value[0]);
			}
		}
		return $smartcardNumber[1];
	}

	
	function findSTBSerialNumber($latestLogFile){
		//PACE STB-SerialNumber in HEX:  00000114  
		$searchstr="PACE STB-SerialNumber in HEX:.*";
		$matches = array();
		$serialNumber = "";
		$lines = file($latestLogFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		
    	$matches = preg_grep("/$searchstr/", $lines);
		$final_value = array();
		
		foreach ($matches as $value) {
			preg_match("/$searchstr/", $value, $final_value);
			if (sizeof($final_value)>0){
				$serialNumber=explode(":",$final_value[0]);
			}
		}
		//$serialNumber=explode(" ",$serialNumber[1]);
		return $serialNumber[1];
	}


	function findFirmwareVersion($latestLogFile){
		#Firmware Version: 2.11.1_RC5-EP1                      #
		$searchstr="Firmware Version:.*";
		$matches = array();
		$firmwareVersion = "";
		$lines = file($latestLogFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		
    	$matches = preg_grep("/$searchstr/", $lines);
		$final_value = array();
		
		foreach ($matches as $value) {
			preg_match("/$searchstr/", $value, $final_value);
			if (sizeof($final_value)>0){
				$firmwareVersion=explode(":",$final_value[0]);
			}
		}
		$firmwareVersion=explode(" ",$firmwareVersion[1]);
		return $firmwareVersion[1];
	}

	function findIMWVersion($latestLogFile){
		$searchstr="IMW version:.*";
		$matches = array();
		$imwVersion = "";
		$lines = file($latestLogFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		
    	$matches = preg_grep("/$searchstr/", $lines);
		$final_value = array();
		
		foreach ($matches as $value) {
			preg_match("/$searchstr/", $value, $final_value);
			if (sizeof($final_value)>0){
				$imwVersion=explode(":",$final_value[0]);
			}
		}
		return $imwVersion[1];
	}


	function findNationality($latestLogFile){
		$searchstr="nat=.*";
		$matches = array();
		$nationality = "";
		$lines = file($latestLogFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		
    	$matches = preg_grep("/$searchstr/", $lines);
		$final_value = array();
		
		foreach ($matches as $value) {
			preg_match("/$searchstr/", $value, $final_value);
			
			if (sizeof($final_value)>0){
				$nationality=explode("=",$final_value[0]);
				//echo $value." >> ".$final_value[0]."<br>";
				
			}
		}
		$nationality=explode(",",$nationality[1]);
	
		return $nationality[0];
	}


	function findLatestLogFile($folderName){
		$latestFile = "";
		$arrayFiles = array();
		$arrayFiles = scandir($folderName,1);
		$numberOfFiles = sizeof($arrayFiles);
		$logFileArray = array();
		$j = 0;
		foreach ($arrayFiles as $filename) {
			if (strpos($filename, 'debug_') !== false) {
				
				$logFileArray[$j] = strval($filename);
				$j = $j + 1;

			}
		}

		//echo $folderName.":: result : ".natsort($logFileArray)."<br>";
		//arsort($logFileArray);
		natsort($logFileArray);
		$count = 1;
		
		foreach ($logFileArray as $item) {
			$result = is_null($item) ? "NULLVAR" : "NOT NULL";
		 	//echo $folderName."/".$item." is null >> ".$result."<br>";
			$latestFile = strval($item);

		}

		return $latestFile;
		//return "debug_00021.log";
	}

	
	


?> 
  </body>
</html>

